
const firstName = document.querySelector("#txt-first-name");
const lastName = document.querySelector("#txt-last-name");
const fullName = document.querySelector("#span-full-name")

const spanFullName = () =>{
	fullName.innerHTML = `${firstName.value} ${lastName.value}`;
}

firstName.addEventListener("keyup", spanFullName);
lastName.addEventListener("keyup", spanFullName);


const color = document.querySelector("#text-color");

const changeColor = () =>{
	console.log(color.value);
	fullName.style.color = color.value;
}
firstName.addEventListener("keyup", changeColor);
lastName.addEventListener("keyup", changeColor);
color.addEventListener("change", changeColor);